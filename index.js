var express = require('express');
const path = require('path');
var app = express();
var bodyParser = require('body-parser');
var request = require('request');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
    extended: true
})); // support encoded bodies

app.set('views', __dirname + '/public/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(express.static('public'));

const dotenv = require('dotenv');
const { env } = require('process');
dotenv.config();

app.get('/', function (req, res) {
    var headers = {
        'Authorization': process.env.KEY,
        'Content-Type': 'application/json'
    }
    // Configure the request
    var options = {
        url: 'https://apitestenv.vnforapps.com/api.security/v1/security',
        method: 'GET',
        headers: headers
    }

    // Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200 || response.statusCode == 201) {


            //esta info tiene que ser obtenida por el cliente
            var jsonDataObj = {
                "channel": "web",
                "amount": 10.5,
                "recurrenceMaxAmount": 100,
                "antifraud": {                
                    "clientIp": "131.161.55.2",
                    "merchantDefineData": {
                        "1": "Test",
                        "20": "Merchant",
                        "33": "Test 3"
                    }
                }
            };
            request.post({
                headers:{
                    'Authorization': body,
                    'Content-Type': 'application/json'
                },
                url: 'https://apitestenv.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/'+process.env.MERCHANTID,
                body: jsonDataObj,
                json: true
            }, function (error, response, body) {                                
                res.render('index.html', {
                    sessionkey:body.sessionKey
                });
            });            
        }
    });

    



});


app.get('/basico', function (req, res) {
    var headers = {
        'Authorization': process.env.KEY,
        'Content-Type': 'application/json'
    }
    // Configure the request
    var options = {
        url: 'https://apitestenv.vnforapps.com/api.security/v1/security',
        method: 'GET',
        headers: headers
    }

    // Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200 || response.statusCode == 201) {
            //esta info tiene que ser obtenida por el cliente
            var jsonDataObj = {
                channel: "web",
                amount: "10.5",
                recurrenceMaxAmount: "100",
                antifraud: {                
                    clientIp: "24.252.107.29",
                    merchantDefineData: {
                        1: "Test",
                        20: "Merchant",
                        33: "Test 3"
                    }
                }
            };   
            console.log(body);         
            request.post({
                headers:{
                    'Authorization': body,
                    'Content-Type': 'application/json'
                },
                url: 'https://apitestenv.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/'+process.env.MERCHANTID,
                body: jsonDataObj,
                json: true
            }, function (error, response, body) {                            
                res.render('basico.html', {
                    sessionkey:body.sessionKey,
                    merchantid:process.env.MERCHANTID,
                    channel:jsonDataObj.channel,
                    purchasenumber: Math.floor(Math.random()*90000) + 10000,
                    amount: jsonDataObj.amount,      
                    recurrence: false            
                });
            });            
        }
    });
});

app.get('/basico2', function (req, res) {
    var headers = {
        'Authorization': process.env.KEY,
        'Content-Type': 'application/json'
    }
    // Configure the request
    var options = {
        url: 'https://apitestenv.vnforapps.com/api.security/v1/security',
        method: 'GET',
        headers: headers
    }

    // Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200 || response.statusCode == 201) {
            //esta info tiene que ser obtenida por el cliente
            var jsonDataObj = {
                channel: "web",
                amount: "10.5",
                recurrenceMaxAmount: "100",
                antifraud: {                
                    clientIp: "24.252.107.29",
                    merchantDefineData: {
                        1: "Test",
                        20: "Merchant",
                        33: "Test 3"
                    }
                }
            };   
            console.log(body);         
            request.post({
                headers:{
                    'Authorization': body,
                    'Content-Type': 'application/json'
                },
                url: 'https://apitestenv.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/'+process.env.MERCHANTID,
                body: jsonDataObj,
                json: true
            }, function (error, response, body) {                            
                res.render('basico2.html', {
                    sessionkey:body.sessionKey,
                    merchantid:process.env.MERCHANTID,
                    channel:jsonDataObj.channel,
                    purchasenumber: Math.floor(Math.random()*90000) + 10000,
                    amount: jsonDataObj.amount,      
                    recurrence: false            
                });
            });            
        }
    });
});


app.get('/basico-recordar-tarjeta', function (req, res) {
    var headers = {
        'Authorization': process.env.KEY,
        'Content-Type': 'application/json'
    }
    // Configure the request
    var options = {
        url: 'https://apitestenv.vnforapps.com/api.security/v1/security',
        method: 'GET',
        headers: headers
    }

    // Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200 || response.statusCode == 201) {
            //esta info tiene que ser obtenida por el cliente
            var jsonDataObj = {
                channel: "web",
                amount: "10.5",
                recurrenceMaxAmount: "100",
                antifraud: {                
                    clientIp: "24.252.107.29",
                    merchantDefineData: {
                        1: "Test",
                        20: "Merchant",
                        33: "Test 3"
                    }
                }
            };            
            request.post({
                headers:{
                    'Authorization': body,
                    'Content-Type': 'application/json'
                },
                url: 'https://apitestenv.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/'+process.env.MERCHANTID,
                body: jsonDataObj,
                json: true
            }, function (error, response, body) {                            
                res.render('recordarTarjeta.html', {
                    sessionkey:body.sessionKey,
                    merchantid:process.env.MERCHANTID,
                    channel:jsonDataObj.channel,
                    purchasenumber:'123456',
                    amount: jsonDataObj.amount,      
                    recurrence: false            
                });
            });            
        }
    });
});

app.get('/basico-recordar-tarjeta-ocultando-cliente', function (req, res) {
    var headers = {
        'Authorization': process.env.KEY,
        'Content-Type': 'application/json'
    }
    // Configure the request
    var options = {
        url: 'https://apitestenv.vnforapps.com/api.security/v1/security',
        method: 'GET',
        headers: headers
    }

    // Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200 || response.statusCode == 201) {
            //esta info tiene que ser obtenida por el cliente
            var jsonDataObj = {
                channel: "web",
                amount: "10.5",
                recurrenceMaxAmount: "100",
                antifraud: {                
                    clientIp: "24.252.107.29",
                    merchantDefineData: {
                        1: "Test",
                        20: "Merchant",
                        33: "Test 3"
                    }
                }
            };            
            request.post({
                headers:{
                    'Authorization': body,
                    'Content-Type': 'application/json'
                },
                url: 'https://apitestenv.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/'+process.env.MERCHANTID,
                body: jsonDataObj,
                json: true
            }, function (error, response, body) {                            
                res.render('basicoClienteOculto.html', {
                    sessionkey:body.sessionKey,
                    merchantid:process.env.MERCHANTID,
                    channel:jsonDataObj.channel,
                    purchasenumber:'123456',
                    amount: jsonDataObj.amount,      
                    recurrence: false,
                     name:'Mario',
                     lastName:'Gallegos',
                     email:'mario@gmail.com'          
                });
            });            
        }
    });
});


app.get('/cuotas', function (req, res) {
    var headers = {
        'Authorization': process.env.KEY,
        'Content-Type': 'application/json'
    }
    // Configure the request
    var options = {
        url: 'https://apitestenv.vnforapps.com/api.security/v1/security',
        method: 'GET',
        headers: headers
    }

    // Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200 || response.statusCode == 201) {
            //esta info tiene que ser obtenida por el cliente
            var jsonDataObj = {
                channel: "web",
                amount: "10.5",
                recurrenceMaxAmount: "100",
                antifraud: {                
                    clientIp: "24.252.107.29",
                    merchantDefineData: {
                        1: "Test",
                        20: "Merchant",
                        33: "Test 3"
                    }
                }
            };            
            request.post({
                headers:{
                    'Authorization': body,
                    'Content-Type': 'application/json'
                },
                url: 'https://apitestenv.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/'+process.env.MERCHANTID,
                body: jsonDataObj,
                json: true
            }, function (error, response, body) {                            
                res.render('cuotas.html', {
                    sessionkey:body.sessionKey,
                    merchantid:process.env.MERCHANTID,
                    channel:jsonDataObj.channel,
                    purchasenumber:'123456',
                    amount: jsonDataObj.amount,      
                    recurrence: false            
                });
            });            
        }
    });
});

app.get('/tokenizacion', function (req, res) {
    var headers = {
        'Authorization': process.env.KEY,
        'Content-Type': 'application/json'
    }
    // Configure the request
    var options = {
        url: 'https://apitestenv.vnforapps.com/api.security/v1/security',
        method: 'GET',
        headers: headers
    }

    // Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200 || response.statusCode == 201) {
            //esta info tiene que ser obtenida por el cliente
            var jsonDataObj = {
                channel: "paycard",
                amount: "10.5",                
                antifraud: {                
                    clientIp: "24.252.107.29",
                    merchantDefineData: {
                        1: "Test",
                        20: "Merchant",
                        33: "Test 3"
                    }
                }
            };            
            request.post({
                headers:{
                    'Authorization': body,
                    'Content-Type': 'application/json'
                },
                url: 'https://apitestenv.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/'+process.env.MERCHANTID,
                body: jsonDataObj,
                json: true
            }, function (error, response, body) {                            
                res.render('tokenizacion.html', {
                    sessionkey:body.sessionKey,
                    merchantid:process.env.MERCHANTID,
                    channel:jsonDataObj.channel,
                    purchasenumber:'123456',
                    amount: jsonDataObj.amount,      
                    recurrence: false ,
                    userBlockId:"ABC85247"   
                });
            });            
        }
    });
});

app.get('/dcc', function (req, res) {
    var headers = {
        'Authorization': process.env.KEY,
        'Content-Type': 'application/json'
    }
    // Configure the request
    var options = {
        url: 'https://apitestenv.vnforapps.com/api.security/v1/security',
        method: 'GET',
        headers: headers
    }

    // Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200 || response.statusCode == 201) {
            //esta info tiene que ser obtenida por el cliente
            var jsonDataObj = {
                channel: "web",
                amount: "10.5",
                recurrenceMaxAmount: "100",
                antifraud: {                
                    clientIp: "24.252.107.29",
                    merchantDefineData: {
                        1: "Test",
                        20: "Merchant",
                        33: "Test 3"
                    }
                }
            };            
            request.post({
                headers:{
                    'Authorization': body,
                    'Content-Type': 'application/json'
                },
                url: 'https://apitestenv.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/'+process.env.MERCHANTID,
                body: jsonDataObj,
                json: true
            }, function (error, response, body) {                            
                res.render('dcc.html', {
                    sessionkey:body.sessionKey,
                    merchantid:process.env.MERCHANTID,
                    channel:jsonDataObj.channel,
                    purchasenumber:'123456',
                    amount: jsonDataObj.amount,      
                    recurrence: false            
                });
            });            
        }
    });
});



app.get('/success', function (req, res) {
    res.sendFile(path.join(__dirname + '/success.html'));
});

app.get('/recurrencia-fija', function (req, res) {
    var headers = {
        'Authorization': process.env.KEY,
        'Content-Type': 'application/json'
    }
    // Configure the request
    var options = {
        url: 'https://apitestenv.vnforapps.com/api.security/v1/security',
        method: 'GET',
        headers: headers
    }

    // Start the request
    request(options, function (error, response, body) {        
        if (!error && response.statusCode == 200 || response.statusCode == 201) {
            //esta info tiene que ser obtenida por el cliente
            var jsonDataObj = {
                channel: "recurrent",
                amount: 10.5,             
                recurrenceMaxAmount: 100,   
                antifraud: {                
                    clientIp: "24.252.107.29",
                    merchantDefineData: {
                        1: "Test",
                        20: "Merchant",
                        33: "Test 3"
                    }
                }
            };                        
            request.post({
                headers:{
                    'Authorization': body,
                    'Content-Type': 'application/json'
                },
                url: 'https://apitestenv.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/'+process.env.MERCHANTID,
                body: jsonDataObj,
                json: true
            }, function (error, response, body) {                            
                res.render('recurrentefijo.html', {
                    sessionkey:body.sessionKey,
                    merchantid:process.env.MERCHANTID,
                    channel:jsonDataObj.channel,
                    purchasenumber:'123456',
                    amount: jsonDataObj.amount,      
                    recurrence: true,
                    recurrencemaxamount:jsonDataObj.recurrenceMaxAmount
                });
            });            
        }
    });
});


app.get('/recurrencia-variable', function (req, res) {
    var headers = {
        'Authorization': process.env.KEY,
        'Content-Type': 'application/json'
    }
    // Configure the request
    var options = {
        url: 'https://apitestenv.vnforapps.com/api.security/v1/security',
        method: 'GET',
        headers: headers
    }

    // Start the request
    request(options, function (error, response, body) {        
        if (!error && response.statusCode == 200 || response.statusCode == 201) {
            //esta info tiene que ser obtenida por el cliente
            var jsonDataObj = {
                channel: "recurrent",
                amount: "1.00",             
                recurrenceMaxAmount: "100",   
                antifraud: {                
                    clientIp: "24.252.107.29",
                    merchantDefineData: {
                        1: "Test",
                        20: "Merchant",
                        33: "Test 3"
                    }
                }
            };                        
            request.post({
                headers:{
                    'Authorization': body,
                    'Content-Type': 'application/json'
                },
                url: 'https://apitestenv.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/'+process.env.MERCHANTID,
                body: jsonDataObj,
                json: true
            }, function (error, response, body) {                            
                res.render('recurrentevariable.html', {
                    sessionkey:body.sessionKey,
                    merchantid:process.env.MERCHANTID,
                    channel:jsonDataObj.channel,
                    purchasenumber:'123456',
                    amount: jsonDataObj.amount,      
                    recurrence: true,
                    recurrencemaxamount:jsonDataObj.recurrenceMaxAmount
                });
            });            
        }
    });
});

app.post('/success', function (req, res) {

    var headers = {
        'Authorization': process.env.KEY,
        'Content-Type': 'application/json'
    }
    // Configure the request
    var options = {
        url: 'https://apitestenv.vnforapps.com/api.security/v1/security',
        method: 'GET',
        headers: headers
    }

    // Start the request
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200 || response.statusCode == 201) {


            //esta info tiene que ser obtenida por el cliente
            // if(req.body.bin == '428078'){
            //     var jsonDataObj = {
            //         "channel": req.body.channel,
            //         "captureType": "manual",
            //         "countable": true,
            //         "order" : {
            //           "tokenId": req.body.transactionToken,
            //           "purchaseNumber": req.body.purchasenumber,
            //           "originalAmount":10.5,
            //           "amount": 5.5,
            //           "currency": "PEN"
            //         },
            //         "sponsored": null
            //       }            
            // }else 
            if (req.body.channel == 'recurrent'){
                var jsonDataObj = {
                    "channel": req.body.channel,
                    "captureType": "manual",
                    "countable": true,
                    "order" : {
                      "tokenId": req.body.transactionToken,
                      "purchaseNumber": req.body.purchasenumber,                      
                      "amount": 10.5,
                      "currency": "PEN",
                      "productId" : 170
                    },
                    "cardHolder" : {
                        "documentNumber" : "E065154",
                        "documentType" : 1
                    },
                    "sponsored": null,
                    "recurrence" : {
                        "amount": "10.5",
                        "beneficiaryFirstName" : "",
                        "beneficiaryId" : Math.floor(Math.random() * 1000000000),
                        "beneficiaryLastName" : "",
                        "frequency" : "monthly",
                        "maxAmount" : 100,
                        "type" : "fixedinitial",
                    }
                  }
            }else{
                var jsonDataObj = {
                    "channel": req.body.channel,
                    "captureType": "manual",
                    "countable": true,
                    "order" : {
                      "tokenId": req.body.transactionToken,
                      "purchaseNumber": req.body.purchasenumber,                      
                      "amount": 10.5,
                      "currency": "PEN"
                    },
                    "sponsored": null
                  }
            }
                        
            request.post({
                headers:{
                    'Authorization': body,
                    'Content-Type': 'application/json'
                },
                url: 'https://apitestenv.vnforapps.com/api.authorization/v3/authorization/ecommerce/'+process.env.MERCHANTID,
                body: jsonDataObj,
                json: true
            }, function (error, response, body) {                                
                res.json(body)
            });            
        }
    });


});







var server = app.listen(process.env.PORT, function () {
    console.log('escuchando al puerto 3001');
});